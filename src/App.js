import './App.css';
import {BrowserRouter as Router, Switch, Link, Route} from "react-router-dom";
import Home from "./Pages/Home/Home";
import DayOne from "./Pages/days-of-creation/day-one/day-one";
import DayTwo from "./Pages/days-of-creation/day-two/day-two";
import DayThree from "./Pages/days-of-creation/day-three/day-three";
import DayFour from "./Pages/days-of-creation/day-four/day-four";
import DayFive from "./Pages/days-of-creation/day-five/day-five";
import DaySix from "./Pages/days-of-creation/day-six/day-six";

function App() {
  return (
    <div className="App">
      <Router>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <Link className="navbar-brand" to="/dayone">Day One</Link>
          <Link className="navbar-brand" to="/daytwo">Day Two</Link>
          <Link className="navbar-brand" to="/daythree">Day Three</Link>
          <Link className="navbar-brand" to="/dayfour">Day Four</Link>
          <Link className="navbar-brand" to="/dayfive">Day Five</Link>
          <Link className="navbar-brand" to="/daysix">Day Six</Link>
        </nav>
        <Switch>
          <Route exact path="/home">
            <Home/>
          </Route>
          <Route path="/dayone">
            <DayOne/>
          </Route>
          <Route path="/daytwo">
            <DayTwo/>
          </Route>
          <Route path="/daythree">
            <DayThree/>
          </Route>
          <Route path="/dayfour">
            <DayFour/>
          </Route>
          <Route path="/dayfive">
            <DayFive/>
          </Route>
          <Route path="/daysix">
            <DaySix/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
