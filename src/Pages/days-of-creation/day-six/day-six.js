import React from "react";
import "./day-six.css"

function DaySix() {
  return (
    <div className="day-six">
      <div className="day-six-scripture">
        Genesis 1 <br/>
        24 And God said, Let the earth bring forth the living creature after his kind, cattle, and creeping thing, and
        beast of the earth after his kind: and it was so. <br/>
        25 And God made the beast of the earth after his kind, and cattle after their kind, and every thing that
        creepeth upon the earth after his kind: and God saw that it was good. <br/>
        26 And God said, Let us make man in our image, after our likeness: and let them have dominion over the fish of
        the sea, and over the fowl of the air, and over the cattle, and over all the earth, and over every creeping
        thing that creepeth upon the earth. <br/>
        27 So God created man in his own image, in the image of God created he him; male and female created he
        them. <br/>
        28 And God blessed them, and God said unto them, Be fruitful, and multiply, and replenish the earth, and subdue
        it: and have dominion over the fish of the sea, and over the fowl of the air, and over every living thing that
        moveth upon the earth. <br/>
        29 And God said, Behold, I have given you every herb bearing seed, which is upon the face of all the earth, and
        every tree, in the which is the fruit of a tree yielding seed; to you it shall be for meat. <br/>
        30 And to every beast of the earth, and to every fowl of the air, and to every thing that creepeth upon the
        earth, wherein there is life, I have given every green herb for meat: and it was so. <br/>
        31 And God saw every thing that he had made, and, behold, it was very good. And the evening and the morning were
        the sixth day. <br/>
      </div>
    </div>
  )
}

export default DaySix;
