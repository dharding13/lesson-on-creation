import React from "react";
import "./day-four.css"

function DayFour() {
  return (
    <div className="day-four">
      <div className="day-four-scripture">
        Genesis 1 <br/>
        14 And God said, Let there be lights in the firmament of the heaven to divide the day from the night; and let
        them be for signs, and for seasons, and for days, and years: <br/>
        15 And let them be for lights in the firmament of the heaven to give light upon the earth: and it was so. <br/>
        16 And God made two great lights; the greater light to rule the day, and the lesser light to rule the night: he
        made the stars also. <br/>
        17 And God set them in the firmament of the heaven to give light upon the earth, <br/>
        18 And to rule over the day and over the night, and to divide the light from the darkness: and God saw that it
        was good. <br/>
        19 And the evening and the morning were the fourth day. <br/>
      </div>
    </div>
  )
}

export default DayFour;
