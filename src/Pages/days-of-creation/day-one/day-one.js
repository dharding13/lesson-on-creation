import React from "react";
import "./day-one.css"
import {Link} from "react-router-dom";

function DayOne() {
  return (
    <div className="day-one">
      <header>
        <Link to="/daytwo">Day Two</Link>
      </header>
      <div className="day-one-scripture">
        Genesis 1 <br/>
        1. In the beginning, God created the heaven and the earth. <br/>
        2. And the earth ws without form, and void; and darkness was upon the face of the deep.
        And the Spirit of God moved upon the face of the waters. <br/>
        3. And God said, Let there be light: and there was light. <br/>
        4. And Got saw the light, that it was good: and God divided the light from the darkness. <br/>
        5. And God called the light Day, and the darkness he called Night. And evening and the morning were the first
        day.
      </div>
    </div>
  )
}

export default DayOne;
