import React from "react";
import "./day-two.css"

function DayTwo() {
  return (
    <div className="day-two">
      <div className="day-two-scripture">
        Genesis 1 <br/>
        6. And God said, Let there be a firmament in the midst of the waters, and let it divide the waters from the
        waters. <br/>
        7. And God made the firmament, and divided the waters which were under the firmament from the waters which were
        above the firmament: and it was so. <br/>
        8. And God called the firmament Heaven. And the evening and the morning were the second day. <br/>
      </div>
    </div>
  )
}

export default DayTwo;
