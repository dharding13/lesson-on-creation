import React from "react";
import "./day-five.css"

function DayFive() {
  return (
    <div className="day-five">
      <div className="day-five-scripture">
        Genesis 1 <br/>
        20 And God said, Let the waters bring forth abundantly the moving creature that hath life, and fowl that may fly
        above the earth in the open firmament of heaven. <br/>
        21 And God created great whales, and every living creature that moveth, which the waters brought forth
        abundantly, after their kind, and every winged fowl after his kind: and God saw that it was good. <br/>
        22 And God blessed them, saying, Be fruitful, and multiply, and fill the waters in the seas, and let fowl
        multiply in the earth. <br/>
        23 And the evening and the morning were the fifth day. <br/>
      </div>
    </div>
  )
}

export default DayFive;
