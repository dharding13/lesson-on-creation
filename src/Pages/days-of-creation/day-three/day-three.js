import React from "react";
import "./day-three.css"

function DayThree() {
  return (
    <div className="day-three">
      <div className="day-three-scripture">
        Genesis 1 <br/>
        9 And God said, Let the waters under the heaven be gathered together unto one place, and let the dry land
        appear: and it was so. <br/>
        10 And God called the dry land Earth; and the gathering together of the waters called he Seas: and God saw that
        it was good. <br/>
        11 And God said, Let the earth bring forth grass, the herb yielding seed, and the fruit tree yielding fruit
        after his kind,
        whose seed is in itself, upon the earth: and it was so. <br/>
        12 And the earth brought forth grass, and herb yielding seed after his kind, and the tree yielding fruit,
        whose seed was in itself, after his kind: and God saw that it was good. <br/>
        13 And the evening and the morning were the third day.
      </div>
    </div>
  )
}

export default DayThree;
